# ohShopify by cmcdonaldca (MODIFIED)

This is a modified version of cmcdonaldca's ohShopify (a Shopify API client for PHP).

For original version and documentation, please visit [ohShopify](https://github.com/cmcdonaldca/ohShopify.php).


## Modification

A function `getAccess()` has been added.

This function is a modification of the `getAccessToken()` function and returns the complete response received from Shopify when a store authorizes an application. It can be used in place of `getAccessToken()` if all the response
information is needed.

```php
<?php

	...
	
	// A modified version of the 'getAccessToken' function that returns all the values received in response from Shopify
	public function getAccess($code) {
		// POST to  POST https://SHOP_NAME.myshopify.com/admin/oauth/access_token
		$url = "https://{$this->shop_domain}/admin/oauth/access_token";
		$payload = "client_id={$this->api_key}&client_secret={$this->secret}&code=$code";
		$response = $this->curlHttpApiRequest('POST', $url, '', $payload, array());
		$response = json_decode($response, true);
		if(isset($response['access_token']))
			return $response;
		return '';
	}
	
	...
	
?>

```

